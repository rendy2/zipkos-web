class CreateCarriers < ActiveRecord::Migration[5.1]
  def change
    create_table :carriers do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.string :position
      t.text :description
      t.string :file

      t.timestamps
    end
  end
end
