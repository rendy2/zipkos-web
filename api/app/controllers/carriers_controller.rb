class CarriersController < ApplicationController
    # PUBLIC #
    def create
        @carrier = Carrier.new(carrier_params)

        if @carrier.save
            render json: {
                status: TRUE,
                message: "Create carrier success!",
                data: @carrier
            }, status: 201
        else
            render json: {
                status: FALSE,
                message: "Create carrier failed!",
                data: @carrier.errors
            }, status: 400
        end
    end

    private

    def carrier_params
        params.permit(:name, :email, :phone, :position, :description, :file)
    end
end
