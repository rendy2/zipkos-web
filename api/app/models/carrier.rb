class Carrier < ApplicationRecord
    mount_uploader :file, FileUploader

    validates :name, presence: true
    validates :email, presence: true
    validates :phone, presence: true
    validates :position, presence: true
    validates :description, presence: true
    validates :file, presence: true
end
