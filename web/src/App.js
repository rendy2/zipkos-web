import React from "react";
import "./App.css";

function App() {
  const [tab, clickTab] = React.useState("tss");

  return (
    <div id="app">
      <h1>We are ZipKos!</h1>
      <h2>Open Positions</h2>

      <div id="tab">
        <button
          onClick={() => clickTab("tss")}
          className={tab === "tss" ? "active" : null}
        >
          Technical Support Specialist
        </button>

        <button
          onClick={() => clickTab("da")}
          className={tab === "da" ? "active" : null}
        >
          Data Analyst
        </button>

        <button
          onClick={() => clickTab("dms")}
          className={tab === "dms" ? "active" : null}
        >
          Digital Marketing Specialist
        </button>
      </div>

      {tab === "tss" ? (
        <div id="tss" className="tabcontent">
          <h3>Technical Support Specialist</h3>
        </div>
      ) : tab === "da" ? (
        <div id="da" className="tabcontent">
          <h3>Data Analyst</h3>
        </div>
      ) : tab === "dms" ? (
        <div id="dms" className="tabcontent">
          <h3>Digital Marketing Specialist</h3>
        </div>
      ) : null}

      <div id="applyNow">
        <h2>Apply Now!</h2>

        <table align="center">
          <tbody>
            <tr>
              <th>Full Name</th>

              <td>
                <input type="text" />
              </td>
            </tr>

            <tr>
              <th>Email</th>

              <td>
                <input type="email" />
              </td>
            </tr>

            <tr>
              <th>Phone Number</th>

              <td>
                <input type="number" />
              </td>
            </tr>

            <tr>
              <th>Position</th>

              <td>
                <select>
                  <option>Technical Support Specialist</option>
                  <option>Data Analyst</option>
                  <option>Digital Marketing Specialist</option>
                </select>
              </td>
            </tr>

            <tr>
              <th>LinkedIn Profile</th>

              <td>
                <input type="text" />
              </td>
            </tr>

            <tr>
              <th>Why it has to be you?</th>

              <td>
                <textarea></textarea>
              </td>
            </tr>

            <tr>
              <th>CV & Portfolio</th>

              <td>
                <input type="file" />
              </td>
            </tr>

            <tr>
              <th>&nbsp;</th>

              <td>
                <button>Send</button>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default App;
